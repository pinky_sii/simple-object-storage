package com.example.simpleobjectstorage

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SimpleObjectStorageApplication

fun main(args: Array<String>) {
    runApplication<SimpleObjectStorageApplication>(*args)
}
