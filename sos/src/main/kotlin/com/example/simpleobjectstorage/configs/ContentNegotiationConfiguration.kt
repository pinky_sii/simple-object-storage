package com.example.simpleobjectstorage.configs

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer



@Configuration
class ContentNegotiationConfiguration: WebMvcConfigurer {

    @Throws(Exception::class)
    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        // Turn off suffix-based content negotiation
        configurer!!.favorPathExtension(false)
    }
}