package com.example.simpleobjectstorage.controllers

import com.example.simpleobjectstorage.modules.objectPart.ObjectPartService
import com.example.simpleobjectstorage.modules.objectStored.ObjectStoredService
import com.example.simpleobjectstorage.modules.objectStored.dto.ObjectStoredCompleteSuccessDto
import com.example.simpleobjectstorage.modules.objectStored.dto.ObjectStoredUploadResponseDto
import com.example.simpleobjectstorage.utils.JsonResponseBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("{bucketName}/{objectName}")
@CrossOrigin
class ObjectController {
    @Autowired
    lateinit var objectStoredService: ObjectStoredService

    @Autowired
    lateinit var objectPartService: ObjectPartService

    @RequestMapping(method = [(RequestMethod.POST)], params = ["create"])
    fun create(@PathVariable("bucketName") bucketName: String,
                     @PathVariable("objectName") objectName: String,
                     request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            objectStoredService.create(bucketName,objectName)
            ResponseEntity.ok(JsonResponseBuilder().put("status", 200).build())
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }

    @RequestMapping(method = [(RequestMethod.PUT)])
    fun multiplePartUpload(@PathVariable("bucketName") bucketName: String,
                           @PathVariable("objectName") objectName: String,
                           @RequestParam("partNumber") partNumber: Int,
                           @RequestHeader("Content-Length") contentLength: Int,
                           @RequestHeader("Content-MD5") contentMd5: String,
                           request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            val respBody = ObjectStoredUploadResponseDto(objectStoredService.upload(bucketName,objectName,partNumber,contentLength,contentMd5,request))
            ResponseEntity.ok(respBody)
        } catch(e: Exception) {
            ResponseEntity.badRequest().body(JsonResponseBuilder()
                    .put("md5", contentMd5)
                    .put("length", contentLength)
                    .put("partNumber", partNumber)
                    .put("error", e.cause!!.message)
                    .build())
        }
    }

    @RequestMapping(method = [(RequestMethod.POST)], params = ["complete"])
    fun completeUpload(@PathVariable("bucketName") bucketName: String,
                       @PathVariable("objectName") objectName: String,
                       request: HttpServletRequest) : ResponseEntity<Any> {
        return try {
            val respBody = ObjectStoredCompleteSuccessDto(objectStoredService.complete(bucketName,objectName))
            ResponseEntity.ok(respBody)
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder()
                    .put("name", objectName)
                    .put("status", e.cause!!.message)
                    .build())
        }
    }

    @RequestMapping(method = [(RequestMethod.DELETE)])
    fun deletePart(@PathVariable("bucketName") bucketName: String,
                           @PathVariable("objectName") objectName: String,
                           @RequestParam("partNumber") partNumber: Int,
                           request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            objectStoredService.deletePart(bucketName,objectName,partNumber)
            ResponseEntity.ok(JsonResponseBuilder().put("status", 200).build())
        } catch (e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }

    @RequestMapping(method = [(RequestMethod.DELETE)], params = ["delete"])
    fun deleteObject(@PathVariable("bucketName") bucketName: String,
                     @PathVariable("objectName") objectName: String,
                     request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            objectStoredService.deleteObject(bucketName,objectName)
            ResponseEntity.ok(JsonResponseBuilder().put("status", 200).build())
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }

    @RequestMapping(method = [(RequestMethod.GET)])
    fun downloadObject(@PathVariable("bucketName") bucketName: String,
                       @PathVariable("objectName") objectName: String,
                       request: HttpServletRequest,
                       response: HttpServletResponse): ResponseEntity<Any> {
        return try {

            var getAllParts = false

            var range = request.getHeader("Range")
            if(range == null) {
                range = "Bytes=0-"
            }
            if(range == ""){
                getAllParts = true
                objectStoredService.download(bucketName,objectName, range, 0, 0,response, getAllParts)
            } else {
                val pattern = Pattern.compile("(-?[0-9]*)-(-?[0-9]*)")
                val matcher = pattern.matcher(range)
                matcher.find()

                val from = matcher.group(1).toInt()

                var to = 0
                to = if(matcher.group(2) == "") { -1 } else { matcher.group(2).toInt()}

                objectStoredService.download(bucketName,objectName, range, from, to,response, getAllParts)
            }

            ResponseEntity.ok().build()
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e).build())
        }
    }

    @RequestMapping(method = [(RequestMethod.PUT)], params = ["metadata"])
    fun updateMetadata(@PathVariable("bucketName") bucketName: String,
                       @PathVariable("objectName") objectName: String,
                       @RequestParam("key") key: String,
                       @RequestBody body: String,
                       request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            objectStoredService.updateMetadata(bucketName,objectName,key,body)
            ResponseEntity.ok(JsonResponseBuilder().put("status", 200).build())
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }

    }

    @RequestMapping(method = [(RequestMethod.DELETE)], params = ["metadata"])
    fun deleteMetadata(@PathVariable("bucketName") bucketName: String,
                       @PathVariable("objectName") objectName: String,
                       @RequestParam("key") key: String,
                       request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            objectStoredService.deleteMetadata(bucketName,objectName,key)
            ResponseEntity.ok(JsonResponseBuilder().put("status", 200).build())
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }

    }

    @RequestMapping(method = [(RequestMethod.GET)], params = ["metadata", "key"])
    fun getMetadataByKey(@PathVariable("bucketName") bucketName: String,
                         @PathVariable("objectName") objectName: String,
                         @RequestParam("key") key: String,
                         request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(objectStoredService.getMetadataByKey(bucketName,objectName,key))
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }

    }

    @RequestMapping(method = [(RequestMethod.GET)], params = ["metadata"])
    fun getAllMetadata(@PathVariable("bucketName") bucketName: String,
                       @PathVariable("objectName") objectName: String,
                       request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(objectStoredService.getAllMetadata(bucketName,objectName))
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }
}