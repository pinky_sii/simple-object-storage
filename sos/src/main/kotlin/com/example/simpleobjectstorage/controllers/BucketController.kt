package com.example.simpleobjectstorage.controllers

import com.example.simpleobjectstorage.modules.bucket.BucketRepository
import com.example.simpleobjectstorage.modules.bucket.BucketService
import com.example.simpleobjectstorage.modules.bucket.dto.BucketCreateResponseDto
import com.example.simpleobjectstorage.modules.bucket.dto.BucketListDto
import com.example.simpleobjectstorage.utils.JsonResponseBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("{bucketName}")
@CrossOrigin
class BucketController {

    @Autowired
    lateinit var bucketService: BucketService

    @Autowired
    lateinit var bucketRepository : BucketRepository

    @RequestMapping(method = [(RequestMethod.POST)], params = ["create"])
    fun createBucket(@PathVariable("bucketName") bucketName: String,
                     request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(BucketCreateResponseDto(bucketService.create(bucketName)))
        } catch(e: Exception) {
            ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }

    @RequestMapping(method = [(RequestMethod.DELETE)], params = ["delete"])
    fun dropBucket(@PathVariable("bucketName") bucketName: String,
                   request: HttpServletRequest): ResponseEntity<Any>{
        return try {
            bucketService.drop(bucketName)
            ResponseEntity.ok(JsonResponseBuilder().put("status", 200).build())
        } catch(e: Exception) {
            return ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }

    @RequestMapping(method = [(RequestMethod.GET)], params = ["list"])
    fun listBucket(@PathVariable("bucketName") bucketName: String,
                   request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(BucketListDto(bucketService.listObjects(bucketName)))
        } catch(e :Exception) {
            ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }

    @RequestMapping(method = [(RequestMethod.GET)], params = ["all"])
    fun listAllBucket(@PathVariable("bucketName") bucketName: String,
                   request: HttpServletRequest): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(bucketRepository.findAll())
        } catch(e :Exception) {
            ResponseEntity.badRequest().body(JsonResponseBuilder().put("status", e.cause!!.message).build())
        }
    }
}