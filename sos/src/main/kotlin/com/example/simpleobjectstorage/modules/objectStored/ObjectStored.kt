package com.example.simpleobjectstorage.modules.objectStored

import com.example.simpleobjectstorage.modules.bucket.Bucket
import com.example.simpleobjectstorage.modules.objectMetadata.ObjectMetadata
import com.example.simpleobjectstorage.modules.objectPart.ObjectPart
import com.example.simpleobjectstorage.utils.BaseEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
class ObjectStored: BaseEntity() {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native",  strategy = "native")
    var id: Long? = null

    @Column
    var name: String? = null

    var eTag: String? = null

    var length: Int? = null   //sum of all parts

    var md5: String? = null   //md5 of all parts

//    var metadata: MutableMap<String, Any> = mutableMapOf()

    @Column
    var uploadTicket: Boolean = false    //upload status


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bucketId")
    @JsonBackReference
    var bucket: Bucket? = null

    @OneToMany(mappedBy = "objectStored", cascade = [CascadeType.ALL])
    @JsonManagedReference
    var parts: MutableList<ObjectPart> = mutableListOf()

    @OneToMany(mappedBy = "objectStored", cascade = [CascadeType.ALL])
    @JsonManagedReference
    var metadata: MutableList<ObjectMetadata> = mutableListOf()
}