package com.example.simpleobjectstorage.modules.objectPart

import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ObjectPartRepository: JpaRepository<ObjectPart, Long> {

    fun findByObjectStoredAndPartNumberAndActiveIsTrue(objectStored: ObjectStored, partNumber: Int): ObjectPart?

    fun findAllByName(name: String): MutableList<ObjectPart>?

    fun findAllByObjectStoredOrderByPartNumberAsc(objectStored: ObjectStored): MutableList<ObjectPart>?

}