package com.example.simpleobjectstorage.modules.objectStored

import com.example.simpleobjectstorage.modules.bucket.Bucket
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ObjectStoredRepository: JpaRepository<ObjectStored, Long> {

    fun findByNameAndBucket(name: String, bucket: Bucket): ObjectStored?
    fun findByName(name: String): ObjectStored?

}