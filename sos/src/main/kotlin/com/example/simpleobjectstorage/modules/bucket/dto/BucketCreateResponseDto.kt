package com.example.simpleobjectstorage.modules.bucket.dto

import com.example.simpleobjectstorage.modules.bucket.Bucket
import org.jetbrains.annotations.NotNull
import java.util.*

class BucketCreateResponseDto {
    @NotNull
    var name: String? = null
    var created: Long? = null
    var modified: Long? = null

    constructor(bucket: Bucket){
        this.name = bucket.name
        this.created = bucket.createdDate!!.time
        this.modified = bucket.modifiedDate!!.time
    }
}