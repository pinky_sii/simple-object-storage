package com.example.simpleobjectstorage.modules.objectMetadata

import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import com.example.simpleobjectstorage.utils.BaseEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
class ObjectMetadata: BaseEntity() {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native",  strategy = "native")
    var id: Long? = null

    var metadataKey: String? = null

    var body: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "objectId")
    @JsonBackReference
    var objectStored: ObjectStored? = null
}