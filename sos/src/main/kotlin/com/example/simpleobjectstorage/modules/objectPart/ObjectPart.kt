package com.example.simpleobjectstorage.modules.objectPart


import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import com.example.simpleobjectstorage.utils.BaseEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
class ObjectPart: BaseEntity() {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native",  strategy = "native")
    var id: Long? = null

    @Column
    var name: String? = null      //object name

    @Column
    var length: Int? = null       //Content-length

    @Column
    var md5: String? = null        //MD5

    @Column
    var partNumber: Int? = null    //part number

    var path: String? = null       //path to file

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "objectId")
    @JsonBackReference
    var objectStored: ObjectStored? = null

}