package com.example.simpleobjectstorage.modules.objectPart

import com.example.simpleobjectstorage.modules.objectStored.ObjectStoredRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ObjectPartService {

    @Autowired
    lateinit var objectPartRepository: ObjectPartRepository

    @Autowired
    lateinit var objectStoredRepository: ObjectStoredRepository

    @Transactional
    fun create(bucketName: String, objectName: String, md5: String, length: Int, partNumber: Int): ObjectPart {
        try {

            //validation: check if bucket, objectstored, and flag

            val objectStored = objectStoredRepository.findByName(objectName)!!
            val objectPart = ObjectPart()
            objectPart.partNumber = partNumber
            objectPart.length = length
            objectPart.md5 = md5

            //save obj to repo
            val savedObjectPart = objectPartRepository.save(objectPart)

            //add part to obj parts
            objectStored.parts.add(savedObjectPart)

            //save obj
            objectStoredRepository.save(objectStored)

            return savedObjectPart

        } catch(e: Exception) {
           throw Exception("error")
        }
    }
}