package com.example.simpleobjectstorage.modules.objectStored.dto

import com.example.simpleobjectstorage.modules.objectPart.ObjectPart
import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import org.jetbrains.annotations.NotNull

class ObjectStoredUploadResponseDto {
    @NotNull
    var partNumber: Int? = null

    var lenght: Int? = null

    var md5: String? = null

    constructor(objectPart: ObjectPart) {
        this.lenght = objectPart.length
        this.md5 = objectPart.md5
        this.partNumber = objectPart.partNumber
    }
}