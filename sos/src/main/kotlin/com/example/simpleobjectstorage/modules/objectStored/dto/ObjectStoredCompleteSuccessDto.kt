package com.example.simpleobjectstorage.modules.objectStored.dto

import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import org.jetbrains.annotations.NotNull

class ObjectStoredCompleteSuccessDto {

    @NotNull

    var eTag: String? = null

    var length: Int? = null

    var name: String? = null


    constructor(objectStored: ObjectStored){
        this.name = objectStored.name
        this.eTag = objectStored.eTag
        this.length = objectStored.length
    }
}