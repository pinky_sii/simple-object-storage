package com.example.simpleobjectstorage.modules.objectMetadata

import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MetedataRepository: JpaRepository<ObjectMetadata, Long> {

    fun findByMetadataKeyAndObjectStored(metadata: String, objectStored: ObjectStored): ObjectMetadata?

    fun findAllByObjectStored(objectStored: ObjectStored): MutableList<ObjectMetadata>?

}