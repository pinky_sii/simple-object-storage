package com.example.simpleobjectstorage.modules.bucket

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.File

@Service
class BucketService {
    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Transactional
    fun create(name: String): Bucket {
        try {
            val buck = bucketRepository.findByName(name)
            if (buck != null) { throw Exception("Bucket already exist!")}
            val bucket = Bucket()
            bucket.name = name
            File("./buckets/${name}").mkdirs()
            return bucketRepository.save(bucket)
        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun drop(name: String) {
        try {
            val bucket = bucketRepository.findByName(name)
            if (bucket != null)
                File("./buckets/$name").deleteRecursively()
                bucketRepository.delete(bucket!!)
        } catch (e: Exception) {
            throw Exception("$name  does not exist!!")
        }
    }

    @Transactional
    fun listObjects(name: String): Bucket {
        try {
            val bucket = bucketRepository.findByName(name)
            if (bucket == null ) { throw Exception("$name  does not exist!!")}
            return bucketRepository.findByName(name)!!
        } catch (e :Exception) {
            throw Exception(e.message)
        }
    }
}