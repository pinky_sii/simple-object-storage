package com.example.simpleobjectstorage.modules.objectStored.dto

import com.example.simpleobjectstorage.modules.bucket.Bucket
import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import org.jetbrains.annotations.NotNull

class ObjectStoredCreateResponseDto {
    @NotNull
    var name: String? = null

    var created: Long? = null

    var modified: Long? = null

    var eTag: String? = null

    constructor(objectStored: ObjectStored){
        this.name = objectStored.name
        this.created = objectStored.createdDate!!.time
        this.modified = objectStored.modifiedDate!!.time
        this.eTag = objectStored.eTag
    }
}