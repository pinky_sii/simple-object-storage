package com.example.simpleobjectstorage.modules.objectStored

import com.example.simpleobjectstorage.modules.bucket.BucketRepository
import com.example.simpleobjectstorage.modules.objectMetadata.MetedataRepository
import com.example.simpleobjectstorage.modules.objectMetadata.ObjectMetadata
import com.example.simpleobjectstorage.modules.objectPart.ObjectPart
import com.example.simpleobjectstorage.modules.objectPart.ObjectPartRepository
import com.example.simpleobjectstorage.utils.JsonResponseBuilder
import org.apache.commons.io.IOUtils
import org.apache.commons.io.output.ByteArrayOutputStream
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.codec.Hex
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.DigestUtils
import java.io.BufferedOutputStream
import java.io.File
import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class ObjectStoredService {
    @Autowired
    lateinit var objectStoredRepository: ObjectStoredRepository

    @Autowired
    lateinit var bucketRepository: BucketRepository

    @Autowired
    lateinit var objectPartRepository: ObjectPartRepository

    @Autowired
    lateinit var metadataRepository: MetedataRepository

    @Transactional
    fun create(bucketName: String, objectName: String): ObjectStored {
        try {

            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket)
            //check if obj already exists
            if (objectStored != null) {throw Exception("$objectName already exists !!")}

            val newObjectStored = ObjectStored()
            newObjectStored.name = objectName
            newObjectStored.bucket = bucket
            val savedObject = objectStoredRepository.save(newObjectStored)

            bucket.files.add(savedObject)
            bucketRepository.save(bucket)
            return savedObject

        } catch(e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun upload(bucketName: String, objectName: String, partNumber: Int, contentLength: Int, contentMd5: String, request: HttpServletRequest): ObjectPart {
        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            val objectPart = objectPartRepository.findByObjectStoredAndPartNumberAndActiveIsTrue(objectStored,partNumber)

            //validate part number: already exists or exceed in range--> InvalidPartNumber
            if (partNumber !in 1..10000 || objectPart != null) {throw Exception("InvalidPartNumber")}

            //check uploadTicket
            if (objectStored!!.uploadTicket) {throw Exception("Uploading ticket is already set to complete")}  //TODO fix bug

            val extension = File(objectName).extension
            val fileName = File(objectName).nameWithoutExtension

            val path = "./buckets/$bucketName/$fileName-$partNumber.$extension"
            val input = request.inputStream
            val fileOut = File(path)
            while (!input.isFinished) {
                fileOut.writeBytes(input.readBytes())
            }

           try {
               //check md5
               val fileM = File(path)
               val contentM = fileM.inputStream()
               val md5 = DigestUtils.md5DigestAsHex(contentM)
               if (md5 != contentMd5) {throw Exception("MD5Mismatched")}

               //check length
               val file = File(path)
               val content = file.inputStream()
               val length = IOUtils.toByteArray(content).size
               if (length != contentLength) {throw Exception("LengthMismatched")}    //TODO .. FIX BUG?

               //check uploadTicket
               if (objectStored!!.uploadTicket) {throw Exception("Uploading ticket is already set to complete")}  //TODO fix bug
           } catch (e: Exception) {
               //remove
               fileOut.delete()
               throw Exception(e.message)
           }

            val newObjectPart = ObjectPart()
            newObjectPart.name = objectName
            newObjectPart.md5 = contentMd5
            newObjectPart.path = path
            newObjectPart.length = contentLength
            newObjectPart.partNumber = partNumber
            newObjectPart.objectStored = objectStored
            val savedObjectPart = objectPartRepository.save(newObjectPart)

            objectStored.parts.add(savedObjectPart)
            objectStoredRepository.save(objectStored)

            return savedObjectPart

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun complete(bucketName: String, objectName: String): ObjectStored {
        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            //compute md 5 and length
            val objectParts = objectPartRepository.findAllByName(objectName)
            var byteOutput = ByteArrayOutputStream()
            var totalLength = 0
            val numOfPart = objectParts!!.size
            for (part in objectParts!!) {
                totalLength += part.length!!
                byteOutput.write(Hex.decode(part.md5))
            }

            var md5ofAll = DigestUtils.md5DigestAsHex(byteOutput.toInputStream())

            val eTag = "$md5ofAll-$numOfPart"

            // update object: set eTag, length, and set flag to complete
            objectStored.uploadTicket = true
            objectStored.eTag = eTag
            objectStored.length = totalLength
            objectStored.md5 = md5ofAll

            return objectStoredRepository.save<ObjectStored?>(objectStored)

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun deletePart(bucketName: String, objectName: String, partNumber: Int) {

        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            val objectPart = objectPartRepository.findByObjectStoredAndPartNumberAndActiveIsTrue(objectStored, partNumber)

            // check upload ticket: Parts cannot be deleted if the object uploading ticket is already set to complete.
            if (objectStored!!.uploadTicket) {throw Exception("Uploading ticket is already set to complete")}  //TODO fix bug

            //validate part number: already exists or exceed in range--> InvalidPartNumber
            if (objectPart == null) {
                throw Exception("InvalidPartNumber")
            }

            //archive, unlink and remove
            objectPart!!.active = false
            objectPart.objectStored = null
            objectStored.parts.remove(objectPart)
            objectPartRepository.save(objectPart)

        } catch(e :Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun deleteObject(bucketName: String, objectName: String) {

        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            //archive
            objectStored.uploadTicket = true
            objectStored.active = false
            objectStored.bucket = null
            for (part in objectStored.parts) {
                part.objectStored = null
                part.active = false
                objectPartRepository.save(part)
            }
            objectStored.parts = mutableListOf()
            bucket.files.remove(objectStored)
            objectStoredRepository.save(objectStored)
            bucketRepository.save(bucket)

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun download(bucketName: String, objectName: String, range: String, from: Int, to: Int, response: HttpServletResponse, getAllParts: Boolean) {
        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            // check complete upload
            if ( ! objectStored.uploadTicket ) { throw Exception("Please, complete the file before download!!") }

            val objectParts = objectPartRepository.findAllByObjectStoredOrderByPartNumberAsc(objectStored)

            val target = response.outputStream
            val baOutputStream = ByteArrayOutputStream()
            var totalSizeRead = 0
            for (p in objectParts!!){
                val fileInputStream = File(p.path).inputStream()
                totalSizeRead += baOutputStream.write(fileInputStream)
                fileInputStream.close()
            }

            val inputStream = baOutputStream.toInputStream()
            if(getAllParts){
                target.write(inputStream.readBytes())
            }else{
                var newTo = to
                if (newTo < 0){
                    newTo = totalSizeRead
                }

                val sizeNeeded = newTo - from
                inputStream.skip(from.toLong())

                for(i in 1..sizeNeeded){
                    target.write(inputStream.read())
                }
            }
            response.setHeader("eTag", objectStored.eTag)

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun updateMetadata(bucketName: String, objectName: String, key: String, body: String) {
        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            val metadata = metadataRepository.findByMetadataKeyAndObjectStored(key,objectStored)

            if (metadata != null ) {
                metadata.body = body
                metadataRepository.save(metadata)
            } else {
                // add
                var objectMetadata= ObjectMetadata()
                objectMetadata.metadataKey = key
                objectMetadata.body = body
                objectMetadata.objectStored = objectStored
                metadataRepository.save(objectMetadata)
                objectStored.metadata.add(objectMetadata)
                objectStoredRepository.save(objectStored)
            }
        } catch (e: Exception) {
            throw Exception(e)
        }
    }

    @Transactional
    fun deleteMetadata(bucketName: String, objectName: String, key: String) {
        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            val metadata = metadataRepository.findByMetadataKeyAndObjectStored(key,objectStored)
            metadataRepository.delete(metadata!!)

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun getMetadataByKey(bucketName: String, objectName: String, key: String): JsonResponseBuilder {
        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            val metadata = metadataRepository.findByMetadataKeyAndObjectStored(key,objectStored)
            val metaKey = metadata!!.metadataKey
            val metaVal = metadata!!.body

            return JsonResponseBuilder().put(metaKey!!,metaVal)

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }

    @Transactional
    fun getAllMetadata(bucketName: String, objectName: String): JsonResponseBuilder {
        try {
            val bucket = bucketRepository.findByName(bucketName) ?: throw Exception("InvalidBucket: $bucketName doesn't exist")

            val objectStored = objectStoredRepository.findByNameAndBucket(objectName,bucket) ?: throw Exception("InvalidObjectName: $objectName doesn't exist")

            var metedatas = metadataRepository.findAllByObjectStored(objectStored)

            var res = JsonResponseBuilder()
            for (meta in metedatas!!) {
                res.put(meta.metadataKey!!, meta.body)
            }

            return res

        } catch (e: Exception) {
            throw Exception(e.message)
        }
    }



}