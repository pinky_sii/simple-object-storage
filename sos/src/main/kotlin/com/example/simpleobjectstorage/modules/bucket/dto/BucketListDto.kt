package com.example.simpleobjectstorage.modules.bucket.dto

import com.example.simpleobjectstorage.modules.bucket.Bucket
import com.example.simpleobjectstorage.modules.objectStored.dto.ObjectStoredCreateResponseDto
import org.jetbrains.annotations.NotNull

class BucketListDto {
    @NotNull
    var name: String? = null

    var created: Long? = null

    var modified: Long? = null

    var objects: List<ObjectStoredCreateResponseDto> = mutableListOf()

    constructor(bucket: Bucket){
        this.name = bucket.name
        this.created = bucket.createdDate!!.time
        this.modified = bucket.modifiedDate!!.time

        val list = mutableListOf<ObjectStoredCreateResponseDto>()
        for (file in bucket.files!!) {
            list.add(ObjectStoredCreateResponseDto(file))
        }
        this.objects = list
    }
}