package com.example.simpleobjectstorage.modules.bucket

import com.example.simpleobjectstorage.modules.objectStored.ObjectStored
import com.example.simpleobjectstorage.utils.BaseEntity
import com.fasterxml.jackson.annotation.JsonManagedReference
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
class Bucket: BaseEntity() {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native",  strategy = "native")
    var id: Long? = null

    @Column
    var name: String? = null

    @OneToMany(mappedBy = "bucket", cascade = [CascadeType.ALL])
    @JsonManagedReference
    var files: MutableList<ObjectStored> = mutableListOf()
}