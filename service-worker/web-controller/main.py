import os
import json
import redis
import requests
from flask import Flask, jsonify, request

app = Flask(__name__)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:thumbnailing'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

@app.route('/create/<bucketname>', methods=['POST'])
def generate_thumbnail(bucketname):
    url = "http://localhost:8080/{}?list".format(bucketname)
    r = requests.get(url)
    if r.status_code == requests.codes.ok :
        objects = r.json()["objects"]
        # print("SPRING:", r.json()["objects"])
        for obj in objects:
            obj_name = obj["name"]
            filename, file_ext = os.path.splitext(obj_name)
            if file_ext not in [".mp4", ".avi", ".mov"]:
                print("Invalid file extension")
                continue

            packed = {"bucketname": bucketname, "objectname": obj_name}
            json_packed = json.dumps(packed)
            print('packed:', json_packed)
            RedisResource.conn.rpush(
                RedisResource.QUEUE_NAME,
                json_packed)
    return jsonify({'status': r.status_code})

@app.route('/create/<bucketname>/<objectname>', methods=['POST'])
def create_thumbnail(bucketname, objectname):
    print(bucketname, objectname)
    url = "http://localhost:8080/{}?list".format(bucketname)
    r = requests.get(url)
    if r.status_code == requests.codes.ok :
        objects = r.json()["objects"]
        for obj in objects:
            obj_name = obj["name"]
            if objectname == obj_name:     #if there exist this obj in bucket
                filename, file_ext = os.path.splitext(obj_name)
                if file_ext not in [".mp4", ".avi", ".mov"]:
                    print("Invalid file extension")
                    continue

                packed = {"bucketname": bucketname, "objectname": obj_name}
                json_packed = json.dumps(packed)
                print('packed:', json_packed)
                RedisResource.conn.rpush(
                    RedisResource.QUEUE_NAME,
                    json_packed)
                return jsonify({'status': r.status_code})
            else:
                return jsonify({'status': requests.codes.bad})
    return jsonify({'status': r.status_code})
