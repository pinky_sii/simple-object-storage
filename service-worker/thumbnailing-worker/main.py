#!/usr/bin/env python3
import os
import logging
import json
import uuid
import redis
import requests
import hashlib

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:thumbnailing'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def execute_worker(log, task):
    # number = task.get('number')
    bucket_name = task.get('bucketname')
    obj_name = task.get('objectname')
    filename, file_ext = os.path.splitext(obj_name)

    if bucket_name and obj_name:
        log.info('Processing %s %s', bucket_name, obj_name)
        url = "http://localhost:8080/{}/{}".format(bucket_name, obj_name)
        headers = {'Range': 'bytes=0-'}
        video = requests.get(url, headers=headers)

        #keep temp file to some path
        path = './{}'.format(obj_name)
        f = open(path, 'wb')
        binary_format = bytearray(video.content)
        f.write(binary_format)
        f.close()

        # generate gif
        outpath = "./{}.gif".format(filename)
        os.system("./make_thumbnail {} {}".format(path, outpath))
        #removet tmp file
        os.remove(path)

        #delete existing thumbnail
        url = 'http://localhost:8080/{}/{}.gif?delete'.format(bucket_name, filename)
        requests.delete(url)

        #create new obj.gif
        url = "http://localhost:8080/{}/{}.gif?create".format(bucket_name, filename)
        requests.post(url)

        #upload gif
        url = "http://localhost:8080/{}/{}.gif?partNumber=1".format(bucket_name, filename)
        file = open(outpath,"rb")
        length = file.seek(0,2)
        file.close

        hash_md5 = hashlib.md5()
        with open(outpath, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
            md5 = hash_md5.hexdigest()

        headers = {'Content-Length': str(length),'Content-MD5': md5}
        data = open(outpath,"rb")
        requests.put(url,headers=headers,data=data)

        #complete
        url = "http://localhost:8080/{}/{}.gif?complete".format(bucket_name, filename)
        requests.post(url)
        os.remove(outpath)
        # factors = [trial for trial in range(1, number+1) if number % trial == 0]
        # log.info('Done, thumbnail = %s', factors)

    else:
        log.info('No number given.')


def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task: execute_worker(named_logging, task))

if __name__ == '__main__':
    main()
